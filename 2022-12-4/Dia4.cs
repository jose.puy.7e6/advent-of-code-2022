﻿using System;

namespace _2022_12_4
{
    internal class Dia4
    {
        static void Main()
        {
            int resposta;

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("     DIA 4");
                Console.ResetColor();
                Console.WriteLine("Quina part vols?\n\n 0- Sortir \n 1- Part 1 \n 2- Part 2");
                resposta = Convert.ToInt32(Console.ReadLine());

                if (resposta == 1)
                {
                    Part1();
                }
                if (resposta == 2)
                {
                    Part2();
                }

            } while (resposta != 0);

        }

        static void Part1()
        {
            Console.Clear();

            string[] strings = new string[4];
            int contador = 0;

            foreach (string line in System.IO.File.ReadLines(@"..\..\..\..\input\input4.txt"))
            {
                strings = line.Split('-', ',');
                

                if (Convert.ToInt32(strings[0]) >= Convert.ToInt32(strings[2]) && Convert.ToInt32(strings[0]) <= Convert.ToInt32(strings[3]) && Convert.ToInt32(strings[1]) >= Convert.ToInt32(strings[2]) && Convert.ToInt32(strings[1]) <= Convert.ToInt32(strings[3]))
                {
                    contador++;
                }
                else if (Convert.ToInt32(strings[2]) >= Convert.ToInt32(strings[0]) && Convert.ToInt32(strings[2]) <= Convert.ToInt32(strings[1]) && Convert.ToInt32(strings[3]) >= Convert.ToInt32(strings[0]) && Convert.ToInt32(strings[3]) <= Convert.ToInt32(strings[1]))
                {
                    contador++;
                }
                
            }
            Console.WriteLine(contador);

            Console.ReadLine();
        }

        static void Part2()
        {
            Console.Clear();

            string[] strings = new string[4];
            int contador = 0;

            foreach (string line in System.IO.File.ReadLines(@"..\..\..\..\input\input4.txt"))
            {
                strings = line.Split('-', ',');


                if (Convert.ToInt32(strings[0]) >= Convert.ToInt32(strings[2]) && Convert.ToInt32(strings[0]) <= Convert.ToInt32(strings[3]) || Convert.ToInt32(strings[1]) >= Convert.ToInt32(strings[2]) && Convert.ToInt32(strings[1]) <= Convert.ToInt32(strings[3]))
                {
                    contador++;
                }
                else if (Convert.ToInt32(strings[2]) >= Convert.ToInt32(strings[0]) && Convert.ToInt32(strings[2]) <= Convert.ToInt32(strings[1]) || Convert.ToInt32(strings[3]) >= Convert.ToInt32(strings[0]) && Convert.ToInt32(strings[3]) <= Convert.ToInt32(strings[1]))
                {
                    contador++;
                }

            }
            Console.WriteLine(contador);

            Console.ReadLine();
        }
    }
}