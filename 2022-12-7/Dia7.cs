﻿using System;

namespace _2022_12_7
{
    internal class Dia7
    {
        static void Main()
        {
            int resposta;

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("     DIA 7");
                Console.ResetColor();
                Console.WriteLine("Quina part vols?\n\n 0- Sortir \n 1- Part 1 \n 2- Part 2");
                resposta = Convert.ToInt32(Console.ReadLine());

                if (resposta == 1)
                {
                    Part1();
                }
                if (resposta == 2)
                {
                    Part2();
                }

            } while (resposta != 0);

        }

        static void Part1()
        {
            Console.Clear();



            Console.ReadLine();
        }

        static void Part2()
        {
            Console.Clear();



            Console.ReadLine();
        }
    }
}
