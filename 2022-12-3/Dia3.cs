﻿using System;
using System.Linq;

namespace _2022_12_3
{
    internal class Dia3
    {
        static void Main()
        {
            int resposta;

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("     DIA 3");
                Console.ResetColor();
                Console.WriteLine("Quina part vols?\n\n 0- Sortir \n 1- Part 1 \n 2- Part 2");
                resposta = Convert.ToInt32(Console.ReadLine());

                if (resposta == 1)
                {
                    Part1();
                }
                if (resposta == 2)
                {
                    Part2();
                }

            } while (resposta != 0);

        }

        static void Part1()
        {
            Console.Clear();

            string input;
            string rev_input = "";
            int resposta;

            int priority = 0;
            char letter = ' ';


            foreach (string line in System.IO.File.ReadLines(@"..\..\..\..\input\input3.txt"))
            {
                input = line;

                foreach (char linia in line) { rev_input = linia + rev_input; }


                for (int i = 0; i < input.Length / 2; i++)
                {
                    for (int j = 0; j < input.Length / 2; j++)
                    {
                        resposta = input[i].CompareTo(rev_input[j]);

                        switch (resposta)
                        {
                            case 0:
                                letter = input[i];
                                break;
                            default:

                                break;
                        }

                    }
                }
                switch (letter)
                {
                    case 'a':
                        priority += 1;
                        break;
                    case 'b':
                        priority += 2;
                        break;
                    case 'c':
                        priority += 3;
                        break;
                    case 'd':
                        priority += 4;
                        break;
                    case 'e':
                        priority += 5;
                        break;
                    case 'f':
                        priority += 6;
                        break;
                    case 'g':
                        priority += 7;
                        break;
                    case 'h':
                        priority += 8;
                        break;
                    case 'i':
                        priority += 9;
                        break;
                    case 'j':
                        priority += 10;
                        break;
                    case 'k':
                        priority += 11;
                        break;
                    case 'l':
                        priority += 12;
                        break;
                    case 'm':
                        priority += 13;
                        break;
                    case 'n':
                        priority += 14;
                        break;
                    case 'o':
                        priority += 15;
                        break;
                    case 'p':
                        priority += 16;
                        break;
                    case 'q':
                        priority += 17;
                        break;
                    case 'r':
                        priority += 18;
                        break;
                    case 's':
                        priority += 19;
                        break;
                    case 't':
                        priority += 20;
                        break;
                    case 'u':
                        priority += 21;
                        break;
                    case 'v':
                        priority += 22;
                        break;
                    case 'w':
                        priority += 23;
                        break;
                    case 'x':
                        priority += 24;
                        break;
                    case 'y':
                        priority += 25;
                        break;
                    case 'z':
                        priority += 26;
                        break;
                    case 'A':
                        priority += 27;
                        break;
                    case 'B':
                        priority += 28;
                        break;
                    case 'C':
                        priority += 29;
                        break;
                    case 'D':
                        priority += 30;
                        break;
                    case 'E':
                        priority += 31;
                        break;
                    case 'F':
                        priority += 32;
                        break;
                    case 'G':
                        priority += 33;
                        break;
                    case 'H':
                        priority += 34;
                        break;
                    case 'I':
                        priority += 35;
                        break;
                    case 'J':
                        priority += 36;
                        break;
                    case 'K':
                        priority += 37;
                        break;
                    case 'L':
                        priority += 38;
                        break;
                    case 'M':
                        priority += 39;
                        break;
                    case 'N':
                        priority += 40;
                        break;
                    case 'O':
                        priority += 41;
                        break;
                    case 'P':
                        priority += 42;
                        break;
                    case 'Q':
                        priority += 43;
                        break;
                    case 'R':
                        priority += 44;
                        break;
                    case 'S':
                        priority += 45;
                        break;
                    case 'T':
                        priority += 46;
                        break;
                    case 'U':
                        priority += 47;
                        break;
                    case 'V':
                        priority += 48;
                        break;
                    case 'W':
                        priority += 49;
                        break;
                    case 'X':
                        priority += 50;
                        break;
                    case 'Y':
                        priority += 51;
                        break;
                    case 'Z':
                        priority += 52;
                        break;
                }
                rev_input = "";
            }

            Console.WriteLine(priority);

            Console.ReadLine();
        }

        static void Part2()
        {
            Console.Clear();

            string[] strings = new string[3];
            int priority = 0;
            char badge1 = ' ';
            int contador = 0;


            foreach (string line in System.IO.File.ReadLines(@"..\..\..\..\input\input3.txt"))
            {
                strings[contador] = line;

                if (strings[2] != null)
                {
                    for (int i = 0; i < strings[0].Length; i++)
                    {
                        if(strings[1].Contains(strings[0][i]))
                            if (strings[2].Contains(strings[0][i]))
                            {
                                badge1 = strings[0][i];
                            }
                    }
                    contador = -1;
                    strings[2] = null;
                }

                switch (badge1)
                {
                    case 'a':
                        priority += 1;
                        break;
                    case 'b':
                        priority += 2;
                        break;
                    case 'c':
                        priority += 3;
                        break;
                    case 'd':
                        priority += 4;
                        break;
                    case 'e':
                        priority += 5;
                        break;
                    case 'f':
                        priority += 6;
                        break;
                    case 'g':
                        priority += 7;
                        break;
                    case 'h':
                        priority += 8;
                        break;
                    case 'i':
                        priority += 9;
                        break;
                    case 'j':
                        priority += 10;
                        break;
                    case 'k':
                        priority += 11;
                        break;
                    case 'l':
                        priority += 12;
                        break;
                    case 'm':
                        priority += 13;
                        break;
                    case 'n':
                        priority += 14;
                        break;
                    case 'o':
                        priority += 15;
                        break;
                    case 'p':
                        priority += 16;
                        break;
                    case 'q':
                        priority += 17;
                        break;
                    case 'r':
                        priority += 18;
                        break;
                    case 's':
                        priority += 19;
                        break;
                    case 't':
                        priority += 20;
                        break;
                    case 'u':
                        priority += 21;
                        break;
                    case 'v':
                        priority += 22;
                        break;
                    case 'w':
                        priority += 23;
                        break;
                    case 'x':
                        priority += 24;
                        break;
                    case 'y':
                        priority += 25;
                        break;
                    case 'z':
                        priority += 26;
                        break;
                    case 'A':
                        priority += 27;
                        break;
                    case 'B':
                        priority += 28;
                        break;
                    case 'C':
                        priority += 29;
                        break;
                    case 'D':
                        priority += 30;
                        break;
                    case 'E':
                        priority += 31;
                        break;
                    case 'F':
                        priority += 32;
                        break;
                    case 'G':
                        priority += 33;
                        break;
                    case 'H':
                        priority += 34;
                        break;
                    case 'I':
                        priority += 35;
                        break;
                    case 'J':
                        priority += 36;
                        break;
                    case 'K':
                        priority += 37;
                        break;
                    case 'L':
                        priority += 38;
                        break;
                    case 'M':
                        priority += 39;
                        break;
                    case 'N':
                        priority += 40;
                        break;
                    case 'O':
                        priority += 41;
                        break;
                    case 'P':
                        priority += 42;
                        break;
                    case 'Q':
                        priority += 43;
                        break;
                    case 'R':
                        priority += 44;
                        break;
                    case 'S':
                        priority += 45;
                        break;
                    case 'T':
                        priority += 46;
                        break;
                    case 'U':
                        priority += 47;
                        break;
                    case 'V':
                        priority += 48;
                        break;
                    case 'W':
                        priority += 49;
                        break;
                    case 'X':
                        priority += 50;
                        break;
                    case 'Y':
                        priority += 51;
                        break;
                    case 'Z':
                        priority += 52;
                        break;
                }
                badge1 = ' ';
                contador++;
            }

            Console.WriteLine(priority);


            Console.ReadLine();
        }
    }
}
