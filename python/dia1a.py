f = open('../input/input.txt')

elf = 0
maxelf = 0

for i in f:
    if i != '\n':
        elf += int(i)
    else:
        if elf > maxelf:
            maxelf = elf
        elf = 0

print(maxelf) 