f = open('../input/input.txt')

elf = 0
maxelf1 = 0
maxelf2 = 0
maxelf3 = 0

for i in f:
    if i != '\n':
        elf += int(i)
    else:
        if elf > maxelf3:
            if elf > maxelf2:
                if elf > maxelf1:
                    maxelf3 = maxelf2
                    maxelf2 = maxelf1
                    maxelf1 = elf
                else:
                    maxelf3 = maxelf2
                    maxelf2 = elf
            else:
                maxelf3 = elf
        elf = 0

print(maxelf1 + maxelf2 + maxelf3) 