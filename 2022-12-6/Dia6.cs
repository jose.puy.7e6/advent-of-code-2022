﻿using System;

namespace _2022_12_6
{
    internal class Dia6
    {
        static void Main()
        {
            int resposta;

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("     DIA 6");
                Console.ResetColor();
                Console.WriteLine("Quina part vols?\n\n 0- Sortir \n 1- Part 1 \n 2- Part 2");
                resposta = Convert.ToInt32(Console.ReadLine());

                if (resposta == 1)
                {
                    Part1();
                }
                if (resposta == 2)
                {
                    Part2();
                }

            } while (resposta != 0);

        }

        static void Part1()
        {
            Console.Clear();

            string input = System.IO.File.ReadAllText(@"..\..\..\..\input\input6.txt");
            char l1 = ' ';
            char l2 = ' ';
            char l3 = ' ';
            char l4 = ' ';

            int valor = 0;

            int num = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (num == 0)
                {
                    l1 = input[i];
                    num++;
                }
                else if (num == 1)
                {
                    l2 = input[i];
                    num++;
                }
                else if (num == 2)
                {
                    l3 = input[i];
                    num++;
                }
                else if (num == 3)
                {
                    l4 = input[i];
                    num++;
                }
                if (num == 4)
                {
                    if (l1 != l2 && l1 != l3 && l1 != l4)
                    {
                        if (l2 != l3 && l2 != l4)
                        {
                            if (l3 != l4)
                            {
                                valor = i;
                                break;
                            }
                        }
                    }
                   
                    l1 = l2;
                    l2 = l3;
                    l3 = l4;
                    l4 = input[i];
                }
            }

            Console.WriteLine(valor);

            Console.ReadLine();
        }

        static void Part2()
        {
            Console.Clear();



            Console.ReadLine();
        }
    }
}
